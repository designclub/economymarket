module.exports = function(grunt) {
    require('time-grunt')(grunt);
    var mode = grunt.option('mode') || 'build';      
    require('load-grunt-config')(grunt,{
        loadGruntTasks: {
            pattern: 'grunt-*',
            config: require('./package.json'),
            scope: mode+'Dependencies'
        },
    });
}