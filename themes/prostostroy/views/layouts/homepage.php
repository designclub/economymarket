<?php $mainAssets = $this->mainAssets; ?>
<?php $this->renderPartial('//layouts/parts/_head')?>
<?php 
    if(YII_DEBUG){
        Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/homepage.css');
    }else{
        Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/homepage.min.css');
    } 
?>
<body>
    <?php $this->renderPartial('//layouts/parts/_header')?>
    <?php $this->renderPartial('//layouts/parts/_search')?>
    <section class="carousel">
        <?php $this->widget('application.modules.gallery.widgets.CarouselWidget',['idGallery'=>2]) ?>
    </section>
    <?php $this->renderPartial('//layouts/parts/_catalog_bar')?>
    <div class="container">
        <div class="free-shipping-guy">
            <a href="<?php echo Yii::app()->createUrl('store/catalog/index') ?> ">
                <img src="<?php echo $mainAssets?>/img/delivery.png">
            </a>
        </div>
        <div class="limiter">
            <!-- section class="lefter">
            </section> -->
        	<article class="contentner">
        		<?php echo $content;?>
                
        	</article>
        </div>
    </div>
    <?php $this->renderPartial('//layouts/parts/_prefooter')?>
    <?php $this->renderPartial('//layouts/parts/_footer')?>
    <div class='notifications bottom-left' id="notifications"></div>
</body>
</html>