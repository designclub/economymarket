<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language; ?>">
<head>

    <?php Yii::app()->controller->widget(
        'vendor.chemezov.yii-seo.widgets.SeoHead',
        array(
            'httpEquivs'         => array(
                'Content-Type'     => 'text/html; charset=utf-8',
                'X-UA-Compatible'  => 'IE=edge,chrome=1',
                'Content-Language' => 'ru-RU'
            ),
            'defaultTitle'       => Yii::app()->getModule('yupe')->siteName,
            'defaultDescription' => Yii::app()->getModule('yupe')->siteDescription,
            'defaultKeywords'    => Yii::app()->getModule('yupe')->siteKeyWords,
        )
    ); ?>

    <?php

        $mainAssets = $this->mainAssets;
        
        if(YII_DEBUG){
            Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/main.css');
            Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/homepage.css');
            Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/shame.css');
            Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/bootstrap/bootstrap-notify.js');
        }else{
            Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/main.min.css');
            Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/homepage.min.css');
            Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/shame.min.css');
            Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/bootstrap/bootstrap-notify.min.js');
        }
    ?>
    <script type="text/javascript">
        var yupeTokenName = '<?php echo Yii::app()->getRequest()->csrfTokenName;?>';
        var yupeToken = '<?php echo Yii::app()->getRequest()->csrfToken;?>';
    </script>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <?php 
        // Favicon
        // $wrongPathes = file_get_contents('.'.$mainAssets.'/favicon/favicon.html');
        // $goodPathes = str_replace('{path}',$mainAssets.'/favicon',$wrongPathes);
        // echo $goodPathes;
        // Yii::app()->getClientScript()->registerLinkTag('shortcut icon', null, Yii::app()->getTheme()->getAssetsUrl() . '/images/favicon.ico');
    ?>
</head>