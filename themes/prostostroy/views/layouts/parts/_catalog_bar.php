<section class="postheader">
	<div class="limiter cateban">
	    <?php
	        $this->widget('application.modules.store.widgets.CategoryLightWidget',[
	        	'htmlOptions'=>[
		        	'class'=>'cateban__menu'
		        ]
	        ]);
	    ?>
	    <?php Yii::app()->clientScript->registerScript('cateban',"
	    			var catebanTimer, catebanTimerClose, hoverTimeout, hoverTimeoutClose;

	    			$('.cateban__item').hover(
	    				function(){
		    				clearTimeout(hoverTimeoutClose);
		    				var e = $(this);
							hoverTimeout = setTimeout(function(){
								console.log('in');
			    				
								e.addClass('cateban__item_active');
			    				$('ul','.cateban__item').not('.cateban__item_active ul').removeClass('active');
			    				$('.cateban__item_active ul').addClass('active');
			    				catebanTimer = setTimeout(function(){
				    				$('.cateban').addClass('cateban_open');
			    				}, 500)
							}, 500);
		    			},
		    			function(){
		    				clearTimeout(hoverTimeout);
					    	clearTimeout(catebanTimer);
					    	$(this).removeClass('cateban__item_active');
		    				hoverTimeoutClose = setTimeout(function(){
		    					console.log('out');
					    		$('ul','.cateban').removeClass('active');
		    					$('.cateban').removeClass('cateban_open');
							}, 500);
		    			}
		    		)
	    	"); ?>
	</div>
</section>