<?php $mainAssets = $this->mainAssets; ?>
<header>
    <div class="limiter">
		 <div class="logo">
            <?php echo  CHtml::link(
                    CHtml::image(
                        Yii::app()->getTheme()->getAssetsUrl() .'/'. Yii::app()->getModule('yupe')->logo,
                        Yii::app()->getModule('yupe')->siteName
                    ),
                Yii::app()->homeUrl); ?>
        </div>
        <?php $this->renderPartial('//layouts/parts/_navbar') ?>
        <div class="header-panel">
            <div class="header-panel__item store-information">
                <div class="basket-widget__image header-panel__image">
                    <img src="<?php echo  $this->mainAssets;?>/icons/icon_pack_c.png" alt="">
                </div>
                <div class="store-information__title header-panel__title">Каталог продукции</div>
                <div class="store-information__link header-panel__link">
                    <?php //echo CHtml::link(Product::getProductCount().' товаров','#'); ?>
                    <?php echo CHtml::link('Товары', Yii::app()->createUrl('store/catalog/index')); ?>
                </div>
            </div>
            <div class="header-panel__item basket-widget" id="shopping-cart-widget">
                <?php $this->widget('application.modules.cart.widgets.ShoppingCartWidget',['id' => 'shopping-cart-widget']); ?>
            </div>
            <div class="header-panel__item user-widget">
                <?php 
                    if (Yii::app()->user->isGuest){
                        $user_url = Yii::app()->createUrl('user/account/login');
                        $label = 'Войти';
                    }
                    else{
                        $user_url = Yii::app()->createUrl('user/profile/profile');
                        $label = 'Кабинет';
                    }
                ?>
                <a href="<?php echo $user_url ?> ">
                    <img class="user-widget__image header-panel__image" src="<?php echo  $this->mainAssets;?>/icons/icon_user_c.png" alt="">
                    <div class="user-widget__title header-panel__title"><?php echo $label?></div>
                </a>
            </div>
        </div>
	</div>
</header>
