<section class="search">
 	<div class="limiter">
    	<?php $this->widget('application.modules.store.widgets.SearchProductWidget'); ?>
 		<div class="minimal-order">
 			<div class="minimal-order__icon">
	 			<i class="fa fa-shopping-basket"></i>
 			</div>
 			<div class="minimal-order__title">
	 			Минимальный заказ
 			</div>
 			<div class="minimal-order__number">
	 			5 000 <i class="fa fa-rub"></i>
 			</div>
 		</div>
 	</div>
</section>