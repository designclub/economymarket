<?php $mainAssets = $this->mainAssets; ?>
<footer id="footer">

	<div class="limiter">
		<div class="footer__item">
			<?php 
                if (Yii::app()->user->isGuest){
                    $user_url = Yii::app()->createUrl('user/account/login');
                    $label = 'Войти в систему';
                }
                else{
                    $user_url = Yii::app()->createUrl('user/profile/profile');
                    $label = 'Войти в личный кабинет';
                }
            ?>
			<div class="footer__item-row">
				<div class="footer__item-icon">
					<a href="<?php echo $user_url ?> "><i class="fa fa-user"></i> <?php echo $label?></a>
				</div>
			</div>

			<div class="footer__item-row">
				<div class="footer__item-icon">
					<a  href="<?php echo Yii::app()->createurl('cart/cart/get') ?>"><i class="fa fa-cart-plus"></i> Корзина товаров</a>
				</div>
			</div>
			<?php 
				$this->widget("application.modules.callback.widgets.CallbackWidget",Callback::getSettings('feedback'));
			?>
			<?php 
				$this->widget("application.modules.callback.widgets.CallbackWidget",Callback::getSettings('footer-call'));
			?>
		</div>
		<div class="footer__item footer__item_menu">
			<?php if (Yii::app()->hasModule('menu')): { ?>
		        <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'katalog','layout'=>'native']); ?>
		    <?php } endif; ?>
		</div>
		<div class="footer__item footer__item_menu">
			<?php if (Yii::app()->hasModule('menu')): { ?>
		        <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'akcii','layout'=>'native']); ?>
		    <?php } endif; ?>
		</div>
		<div class="footer__item footer__item_menu">
			<?php if (Yii::app()->hasModule('menu')): { ?>
		        <?php $this->widget('application.modules.menu.widgets.MenuWidget', ['name' => 'menyu-dlya-pokupatelya','layout'=>'native']); ?>
		    <?php } endif; ?>
		</div>
		<div class="footer__item footer__item_right">
			<div class="social">
				<a class="social__link" href="vk.com" target="_blanc"><i class="fa fa-vk"></i></a>		
				<a class="social__link" href="facebook.com" target="_blanc"><i class="fa fa-facebook"></i></a>		
				<a class="social__link" href="odnoklassniki.ru" target="_blanc"><i class="fa fa-odnoklassniki"></i></a>
				<br>
				<a class="social__link" href="youtube.com" target="_blanc"><i class="fa fa-youtube"></i></a>		
				<a class="social__link" href="twitter.com" target="_blanc"><i class="fa fa-twitter"></i></a>		
				<a class="social__link" href="instagram.com" target="_blanc"><i class="fa fa-instagram"></i></a>		
			</div>
		</div>
		<!-- <div class="copyright">2014-<?php //echo Yii::app()->dateFormatter->format('yyyy',time()); ?> © Мир вдохновений</div> -->
		<!-- <div class="developer">
			<a href="http://designclub56.ru" target="_blanc" class="link">
				<div class="text">
				        <span class="path-change">Разработка сайтов</span><br /> в Оренбурге
			    </div>
				<?php //echo file_get_contents('.'.$mainAssets.'/svg/dc.svg'); ?>
		   </a>
		</div> -->
	</div>
</footer>
<div class="postfooter">
	<div class="limiter">
		<?php $this->widget(
		    "application.modules.contentblock.widgets.ContentBlockWidget",
		    array("code" => "postufuter"));
		?>
	</div>
</div>
<div class="copyrighter">
	<div class="limiter">
		© 2014-<?php echo Yii::app()->dateFormatter->format('yyyy',time()); ?> © Экономаркет
	</div>
</div>
<?php //$this->widget("application.modules.contentblock.widgets.ContentBlockWidget",["code" => "schetchik"]);?>

<?php 
	if(YII_DEBUG){
		echo'<link rel="stylesheet" href="http://yandex.st/highlightjs/8.2/styles/github.min.css">
			<script src="http://yastatic.net/highlightjs/8.2/highlight.min.js"></script>';
	}
?>



