<?php $mainAssets = $this->mainAssets; ?>
<?php $this->renderPartial('//layouts/parts/_head')?>

<body>
    <?php $this->renderPartial('//layouts/parts/_header')?>
    <?php $this->renderPartial('//layouts/parts/_search')?>
    <?php $this->renderPartial('//layouts/parts/_catalog_bar')?>
    <div class="container">
        <div class="free-shipping-guy">
            <a href="<?php echo Yii::app()->createUrl('store/catalog/index') ?> ">
                <img src="<?php echo $mainAssets?>/img/delivery.png">
            </a>
        </div>
        <div class="limiter">
            <!-- section class="lefter">
            </section> -->
        	<article class="contentner">
        		<?php echo $content;?>
        	</article>
        </div>
    </div>
    <?php $this->renderPartial('//layouts/parts/_footer')?>
    <div class='notifications bottom-left' id="notifications" style="position: fixed; left: 10%; bottom: 20%; display: inline-block;"></div>
</body>
</html>