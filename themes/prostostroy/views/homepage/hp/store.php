<?php

$this->layout = '//layouts/homepage';

$mainAssets = $this->mainAssets;

if(YII_DEBUG){
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/catalog-main-page.css');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store/store.js');
}else{
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/store-index.min.css');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store/store.min.js');
}
$this->title = ['Каталог продукции',Yii::app()->getModule('yupe')->siteName];
$this->description = '';
$this->keywords = '';
/* @var $category StoreCategory */

$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog")];

?>

<div class="categories">
    <?php $this->widget('yupe\widgets\YFlashMessages'); ?>

    <?php 
    $this->widget(
        'bootstrap.widgets.TbListView',
        [
            'dataProvider' => $categories,
            'itemView' => '_view',
            'summaryText' => '',
            'enableHistory' => true,
            'cssFile' => false,
            'template' => '{items}'
        ]
    ); ?>
    <div class="categories__link_wrap">
        <?php echo CHtml::link('Посмотреть все категории', Yii::app()->createUrl('store/catalog/index'), ['class' => 'categories__link']) ?>
    </div>
</div>



