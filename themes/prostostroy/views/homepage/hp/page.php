<?php
$mainAssets = $this->mainAssets;
$this->title = [$page->title, Yii::app()->getModule('yupe')->siteName];
$this->breadcrumbs = [
    Yii::t('HomepageModule.homepage', 'Pages'),
    $page->title
];
$this->metaDescription = $page->description;
$this->metaKeywords =  $page->keywords;
?>
<section class="carousel">
      <?php $this->widget('application.modules.gallery.widgets.CarouselWidget') ?>
</section>

<h3><?php echo $page->title; ?></h3>

<?php echo $page->body; ?>

<div class="infographics">
	<div class="info-block">
		<div class="icon product"></div>
		<div class="description">
			Товары только<br />
			от извесных производителей
		</div>
	</div>
	<div class="info-block">
		<div class="icon dayly"></div>
		<div class="description">
			Работаем круглосуточно <br />
			7 дней в неделю
		</div>
	</div>
	<div class="info-block">
		<div class="icon prices"></div>
		<div class="description">
			Вас удивят<br />
			наши низкие цены
		</div>
	</div>
</div>
<?php $this->widget('application.modules.store.widgets.SpecialProductWidget') ?>