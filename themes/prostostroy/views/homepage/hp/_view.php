<?php //$categoryUrl = Yii::app()->createUrl('/store/catalog/category', ['path' => CHtml::encode($data->slug)]); ?>
<div class="category__item">
    <h2 class="category__item_title">
        <?php echo CHtml::link($data->name, $data->getUrl(), ['class' => 'category__item_link']); ?>
    </h2>
    <?php foreach ($data->children as $key => $child): ?>
        <?php if ($key < 6) : ?>
            <div class="subcat__item">
                <a href="<?php echo $child->getUrl(); ?>" class="subcat__item_link with-image">
                    <img src="<?php echo $child->image; ?>" class="subcat__item_image"/>
                </a>
                <p class="subcat__item_descr">
                    <a href="<?php echo $child->getUrl(); ?>" class="subcat__item_link">
                        <?php echo trim($child->name); ?>
                    </a>
                    <!-- <span class="subcat__item_count">
                         <?php //echo mt_rand(1, 1500) ?>
                    </span> -->
                </p>
            </div>
        <?php else : ?>
            <span class="subcat__item_without-image">
                <a href="<?php echo $child->getUrl(); ?>" class="subcat__item_link">
                     <?php echo trim($child->name); ?>
                </a>
                <!-- <span class="subcat__item_count">
                    <?php //echo mt_rand(1, 1500) ?>
                </span> -->
            </span>
        <?php endif; ?>
    <?php endforeach ?>
</div>
