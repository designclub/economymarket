<?php Yii::import('application.modules.news.NewsModule'); ?>
<?php $mainAssets = $this->controller->mainAssets; ?>

<div class='last-news'>
    <div class='last-news__title'>
        <img src="<?php echo $mainAssets ?>/icons/icon_news.png"><br />
        Новости магазина
    </div>
    <div class='last-news__contetn'>
        <?php if (isset($models) && $models != []): ?>
            <ul class="last-news__list">
                <?php foreach ($models as $model): ?>
                    <li class="last-news__item">
                    <?php 
                       if($model->image):?>
                            <div class="last-news__item-image">
                                <img src="<?php echo $model->getImageUrl(180,110) ?> ">
                            </div>
                        <?php endif; ?>
                        <div class="last-news__item-title">
                            <div class="last-news__item-title-cutter">
                                <?php echo CHtml::link(
                                        $model->title,
                                        ['/news/news/show/', 'slug' => $model->slug]
                                    ); ?>
                            </div>
                        </div>
                        <div class="last-news__item-short-descr"><?php echo $model->short_text?> </div>
                        <?php echo CHtml::link(
                                        'Читать',
                                        ['/news/news/show/', 'slug' => $model->slug],
                                        [
                                            'class' => 'last-news__item-button'
                                        ]
                                    ); ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>
