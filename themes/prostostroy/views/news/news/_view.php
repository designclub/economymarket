<?php
/* @var $data News */
?>
<h4><?php echo CHtml::link(CHtml::encode($data->title), $data->getUrl()); ?></h4>
<?php if($data->image): ?>
	<img class="img-thumbnail" src="<?php echo $data->getImageUrl(100,100); ?>" style="float:left; margin: 0 20px 20px 0">
<?php endif; ?>
<p> <?php echo $data->short_text; ?></p>
<p class="text-right"><?php echo CHtml::link('читать', $data->getUrl(), ['class' => 'btn btn-default btn-just-news']); ?></p>
<hr>
