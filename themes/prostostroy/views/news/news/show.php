<?php
/**
 * Отображение для ./themes/default/views/news/news/news.php:
 *
 * @category YupeView
 * @package  YupeCMS
 * @author   Yupe Team <team@yupe.ru>
 * @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 * @link     http://yupe.ru
 *
 * @var $this NewsController
 * @var $model News
 **/
?>
<?php
    $mainAssets = $this->mainAssets;
    $this->title = [$model->title, Yii::app()->getModule('yupe')->siteName];
    $this->metaDescription = $model->description;
    $this->metaKeywords = $model->keywords;
?>

<?php
$this->breadcrumbs = [
    Yii::t('NewsModule.news', 'News') => ['/news/news/index'],
];
?>
<h1 class="page-title">
    Новости
</h1>
<?php $this->widget('zii.widgets.CBreadcrumbs',
        [
            'links'=>$this->breadcrumbs,
            'encodeLabel'=>false,
            'tagName'=>'div',
            'separator'=>'//',
            'htmlOptions'=>[
                            'class'=>'breadcrumb'
                            ]
        ])
?>
<div class="col-sm-12">
    <h4><strong><?php echo CHtml::encode($model->title); ?></strong></h4>
</div>
<div class="col-sm-12">
    <?php echo $model->full_text; ?>
</div>
<div class="col-sm-12">
    <p class="pull-right"><?php echo $model->date; ?></p>
</div>
