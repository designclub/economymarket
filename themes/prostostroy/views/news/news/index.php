<?php
    $mainAssets = $this->mainAssets;
    $this->title = [Yii::t('NewsModule.news', 'News'), Yii::app()->getModule('yupe')->siteName];
    $this->breadcrumbs = [Yii::t('NewsModule.news', 'News')];
?>

<h1 class="page-title">
    Новости
</h1>
<?php $this->widget('zii.widgets.CBreadcrumbs',
        [
            'links'=>$this->breadcrumbs,
            'encodeLabel'=>false,
            'tagName'=>'div',
            'separator'=>'//',
            'htmlOptions'=>[
                            'class'=>'breadcrumb'
                            ]
        ])
?>
<div class="col-xs-12">
<?php $this->widget(
    'bootstrap.widgets.TbListView',
    [
        'dataProvider' => $dataProvider,
        'itemView'     => '_view',
        'summaryText'	=>	'',
        'enableHistory' => true,
        'cssFile' => false,
        'pager' => [
            'cssFile' => false,
            'htmlOptions' => ['class' => 'pagination'],
            'header' => '',
            'firstPageLabel' => '',
            'lastPageLabel' => '',
            'nextPageLabel' => '',
            'prevPageLabel' => '',
        ],
    ]
); ?>
</div>