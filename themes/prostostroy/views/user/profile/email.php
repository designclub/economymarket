<?php
$mainAssets = $this->mainAssets;
$this->title = [Yii::t('UserModule.user', 'Change email'), Yii::app()->getModule('yupe')->siteName];
$this->breadcrumbs = [
    Yii::t('UserModule.user', 'User profile') => ['/user/profile/profile'],
    Yii::t('UserModule.user', 'Change email')
];

$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id'                     => 'profile-email-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'htmlOptions'            => [
            'class' => 'well',
        ]
    ]
);
?>
<div class="page-description">
        <h3 class="page-title">
            Изменения email
        </h3>
        <div class="give-me-more">
            <?php $this->widget('zii.widgets.CBreadcrumbs',
                [
                    'links'=>$this->breadcrumbs,
                    'tagName'=>'ul',
                    'separator'=>'',
                    'activeLinkTemplate'=>'<li><a href="{url}"><div class="separator">'.file_get_contents('.'.$mainAssets.'/svg/icon-arrow.svg').'</div>{label}</a></li>',
                    'inactiveLinkTemplate'=>'<li class="active"><div class="separator">'.file_get_contents('.'.$mainAssets.'/svg/icon-arrow.svg').'</div>{label}</li>',
                    'htmlOptions'=>[
                                    'class'=>'breadcrumb'
                                    ]
                ]) ?>
        </div>
    <div class="page-description-text">
        <?php $this->widget(
            "application.modules.contentblock.widgets.ContentBlockWidget",
            ["code" => "opisanie-dlya-stranicy-izmeneniya-email"]);
        ?>
    </div>
</div>
<?php echo $form->errorSummary($model); ?>

<div class="row">
    <div class="col-xs-6">
        <?php echo $form->textFieldGroup(
            $model,
            'email',
            [
                'widgetOptions' => [
                    'htmlOptions' => [
                        'autocomplete' => 'off',
                    ],
                ],
            ]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            [
                'buttonType' => 'submit',
                'context'    => 'primary',
                'label'      => Yii::t('UserModule.user', 'Change email'),
            ]
        ); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
