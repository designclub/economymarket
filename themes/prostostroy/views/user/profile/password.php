<?php
/* @var $model ProfileForm */
$mainAssets = $this->mainAssets;
$this->title = [Yii::t('UserModule.user', 'Change password'), Yii::app()->getModule('yupe')->siteName];
$this->breadcrumbs = [
    Yii::t('UserModule.user', 'User profile') => ['/user/profile/profile'],
    Yii::t('UserModule.user', 'Change password')
];

Yii::app()->clientScript->registerScript(
    'show-password',
    "$(function () {
    $('#show_pass').click(function () {
        $('#ProfilePasswordForm_password').prop('type', $(this).prop('checked') ? 'text' : 'password');
        $('#ProfilePasswordForm_cPassword').prop('type', $(this).prop('checked') ? 'text' : 'password');
    });
});"
);

$form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id'                     => 'profile-password-form',
        'enableAjaxValidation'   => false,
        'enableClientValidation' => true,
        'htmlOptions'            => [
            'class' => 'well',
        ]
    ]
);
?>
<div class="page-description">
        <h3 class="page-title">
            Смена пароля
        </h3>
        <div class="give-me-more">
            <?php $this->widget('zii.widgets.CBreadcrumbs',
                [
                    'links'=>$this->breadcrumbs,
                    'tagName'=>'ul',
                    'separator'=>'',
                    'activeLinkTemplate'=>'<li><a href="{url}"><div class="separator">'.file_get_contents('.'.$mainAssets.'/svg/icon-arrow.svg').'</div>{label}</a></li>',
                    'inactiveLinkTemplate'=>'<li class="active"><div class="separator">'.file_get_contents('.'.$mainAssets.'/svg/icon-arrow.svg').'</div>{label}</li>',
                    'htmlOptions'=>[
                                    'class'=>'breadcrumb'
                                    ]
                ]) ?>
        </div>
    <div class="page-description-text">
        <?php $this->widget(
            "application.modules.contentblock.widgets.ContentBlockWidget",
            ["code" => "opisanie-dlya-stranicy-smeny-parolya"]);
        ?>
    </div>
</div>
<?php echo $form->errorSummary($model); ?>

<div class="row">
    <div class="col-xs-6">
        <?php echo $form->passwordFieldGroup(
            $model,
            'password',
            ['widgetOptions' => ['htmlOptions' => ['autocomplete' => 'off']]]
        ); ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-6">
        <?php echo $form->passwordFieldGroup(
            $model,
            'cPassword',
            ['widgetOptions' => ['htmlOptions' => ['autocomplete' => 'off']]]
        ); ?>

    </div>
    <div class="col-xs-6">
        <br/>
        <label class="checkbox">
            <input type="checkbox" value="1" id="show_pass"> <?php echo Yii::t('UserModule.user', 'show password') ?>
        </label>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            [
                'buttonType' => 'submit',
                'context'    => 'primary',
                'label'      => Yii::t('UserModule.user', 'Change password'),
            ]
        ); ?>
    </div>
</div>
<?php $this->endWidget(); ?>
