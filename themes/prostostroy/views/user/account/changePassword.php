<?php
$mainAssets = $this->mainAssets;
$this->title = [Yii::t('UserModule.user', 'Password recovery'), Yii::app()->getModule('yupe')->siteName];
$this->breadcrumbs = [Yii::t('UserModule.user', 'Password recovery')];
?>

<?php $this->widget('yupe\widgets\YFlashMessages'); ?>

<?php $form = $this->beginWidget(
    'bootstrap.widgets.TbActiveForm',
    [
        'id'          => 'login-form',
        'type'        => 'vertical',
        'htmlOptions' => [
            'class' => 'well',
        ]
    ]
); ?>
<div class="page-description">
        <h3 class="page-title">
            Смена пароля
        </h3>
        <div class="give-me-more">
           <?php $this->widget('zii.widgets.CBreadcrumbs',
                [
                    'links'=>$this->breadcrumbs,
                    'tagName'=>'ul',
                    'separator'=>'',
                    'activeLinkTemplate'=>'<li><a href="{url}"><div class="separator">'.file_get_contents('.'.$mainAssets.'/svg/icon-arrow.svg').'</div>{label}</a></li>',
                    'inactiveLinkTemplate'=>'<li class="active"><div class="separator">'.file_get_contents('.'.$mainAssets.'/svg/icon-arrow.svg').'</div>{label}</li>',
                    'htmlOptions'=>[
                                    'class'=>'breadcrumb'
                                    ]
                ]) ?>
        </div>
    <div class="page-description-text">
        <?php $this->widget(
            "application.modules.contentblock.widgets.ContentBlockWidget",
            ["code" => "opisanie-dlya-stranicy-smeny-parolya"]);
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php echo $form->errorSummary($model); ?>

    </div>
</div>

<div class='row'>
    <div class="col-xs-6">
        <?php echo $form->passwordFieldGroup($model, 'password'); ?>
    </div>
</div>

<div class='row'>
    <div class="col-xs-6">
        <?php echo $form->passwordFieldGroup($model, 'cPassword'); ?>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <?php
        $this->widget(
            'bootstrap.widgets.TbButton',
            [
                'buttonType' => 'submit',
                'context'    => 'primary',
                'icon'       => 'glyphicon glyphicon-signin',
                'label'      => Yii::t('UserModule.user', 'Change password'),
            ]
        ); ?>
    </div>
</div>

<?php $this->endWidget(); ?>
