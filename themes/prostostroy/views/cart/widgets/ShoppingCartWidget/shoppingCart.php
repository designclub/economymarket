<?php $mainAssets = $this->controller->mainAssets;
    if (Yii::app()->cart->isEmpty()){
        $count = 0;
        $val = 'Нет товаров';
    }else{
        $count = Yii::app()->cart->getItemsCount();
        $link_href = Yii::app()->createUrl('store/catalog/index');
        $val = $count + 'товаров';
    }
    
?>
<div id="cart-widget" href="<?php echo Yii::app()->createurl('cart/cart/get') ?>" data-cart-widget-url="<?= Yii::app()->createUrl('/cart/cart/widget');?>">
    <a href='<?php echo Yii::app()->createUrl('cart/cart/index'); ?>'>
        <div class="basket-widget__image header-panel__image">
            <img src="<?php echo  $mainAssets;?>/icons/icon_cart_c.png" alt="">
        </div> 
        <div class="basket-widget__title header-panel__title">Корзина</div>
        <div class="basket-widget__link header-panel__link">
            <div id="itemsInBasket" class="in-basket"><span  data-count="<?php echo $count;?> "><?php echo $val ?></span></div>
        </div>
    </a>
</div>

