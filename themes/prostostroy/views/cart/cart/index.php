<?php
/* @var $this CartController */
/* @var $positions Product[] */
/* @var $order Order */
/* @var $coupons Coupon[] */
// Yii::app()->getClientScript()->registerCssFile(
//     Yii::app()->getModule('cart')->getAssetsUrl(). '/css/cart-frontend.css'
// );

    $mainAssets = Yii::app()->getTheme()->getAssetsUrl();
    if(YII_DEBUG){
        Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store/store.js');
    }else{
        Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store/store.min.js');
    }
    
    $this->title = [Yii::t('CartModule.cart', 'Cart'), Yii::app()->getModule('yupe')->siteName];
    $this->metaDescription = '';
    $this->metaKeywords =  '';
    $this->breadcrumbs = [Yii::t("CartModule.cart", 'Cart')];
?>


<script type="text/javascript">
    var yupeCartDeleteProductUrl = '<?php echo Yii::app()->createUrl('/cart/cart/delete/')?>';
    var yupeCartUpdateUrl = '<?php echo Yii::app()->createUrl('/cart/cart/update/')?>';
    var yupeCartWidgetUrl = '<?php echo Yii::app()->createUrl('/cart/cart/widget/')?>';
</script>
<div class="page-description">
        <h3 class="page-title">
            Корзина
        </h3>
        <div class="give-me-more">
             <?php $this->widget('zii.widgets.CBreadcrumbs',
                    [
                        'links'=>$this->breadcrumbs,
                        'encodeLabel'=>false,
                        'tagName'=>'div',
                        'separator'=>'//',
                        'htmlOptions'=>[
                                        'class'=>'breadcrumb'
                                        ]
                    ])
            ?>
        </div>
<!--     <div class="page-description-text">
    <?php /*$this->widget(
        "application.modules.contentblock.widgets.ContentBlockWidget",
        ["code" => "opisanie-dlya-korziny"]);
    */?>
</div> -->
</div>
<?php $this->widget('yupe\widgets\YFlashMessages'); ?>
<div class="row">
<?php
        $form = $this->beginWidget(
            'bootstrap.widgets.TbActiveForm',
            [
                'action' => ['/order/order/create'],
                'id' => 'order-form',
                'enableAjaxValidation' => false,
                'enableClientValidation' => true,
                'clientOptions' => [
                    'validateOnSubmit' => true,
                    'validateOnChange' => true,
                    'validateOnType' => false,
                    'beforeValidate' => 'js:function(form){$(form).find("button[type=\'submit\']").prop("disabled", true); return true;}',
                    'afterValidate' => 'js:function(form, data, hasError){$(form).find("button[type=\'submit\']").prop("disabled", false); return !hasError;}',
                ],
                'htmlOptions' => [
                    'hideErrorMessage' => false,
                    'class' => 'order-form',
                ]
            ]
        );
        ?>
    <?php if (Yii::app()->cart->isEmpty()): ?>
        <h1><?php echo Yii::t("CartModule.cart", "Cart is empty"); ?></h1>
        Сейчас у Вас в корзине нет товаров, чтобы купить понравившуюся вешь, добавте её в корзину и оформите заказ. 
        <a href="<?php echo Yii::app()->createUrl('store/catalog/index'); ?>" class="btn btn-link">
                            <?php echo Yii::t("CartModule.cart", "В каталог"); ?>
                        </a>
    <?php else: ?>
        
        <table class="table">
            <thead>
                <tr>
                    <th><?php echo Yii::t("CartModule.cart", "Product"); ?></th>
                    <th><?php echo Yii::t("CartModule.cart", "Amount"); ?></th>
                    <th class="text-center"><?php echo Yii::t("CartModule.cart", "Price"); ?></th>
                    <th class="text-center"><?php echo Yii::t("CartModule.cart", "Sum"); ?></th>
                    <th> </th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($positions as $position): ?>
                    <tr>
                        <td class="col-sm-5">
                            <?php $positionId = $position->getId(); ?>
                            <?php echo CHtml::hiddenField('OrderProduct[' . $positionId . '][product_id]', $position->id); ?>
                            <input type="hidden" class="position-id" value="<?php echo $positionId; ?>"/>

                            <div class="media">
                                <?php $productUrl = Yii::app()->createUrl('store/catalog/show', ['name' => $position->slug]); ?>
                                <a class="img-thumbnail pull-left" href="<?php echo $productUrl; ?>">
                                    <img class="media-object" src="<?php echo $position->getProductModel()->getImageUrl(72, 72); ?>">
                                </a>

                                <div class="media-body">
                                    <h4 class="media-heading">
                                        <a href="<?php echo $productUrl; ?>"><?php echo $position->name; ?></a>
                                    </h4>
                                    <?php foreach ($position->selectedVariants as $variant): ?>
                                        <h6><?php echo $variant->attribute->title; ?>: <?php echo $variant->getOptionValue(); ?></h6>
                                        <?php echo CHtml::hiddenField('OrderProduct[' . $positionId . '][variant_ids][]', $variant->id); ?>
                                    <?php endforeach; ?>
                                    <span>
                                        <?php echo Yii::t("CartModule.cart", "Status"); ?>:
                                    </span>
                                    <span class="text-<?php echo $position->quantity > 0 ? "success" : "warning"; ?>">
                                        <strong><?php echo $position->quantity > 0 ? Yii::t("CartModule.cart", "In stock") : Yii::t("CartModule.cart", "Not in stock"); ?></strong>
                                    </span>
                                </div>
                            </div>
                        </td>
                        <td class="col-sm-2">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button class="btn btn-default cart-quantity-decrease" type="button" data-target="#cart_<?php echo $positionId; ?>">-</button>
                                </div>
                                <div class="fantom">
                                <?php echo CHtml::textField(
                                    'OrderProduct[' . $positionId . '][quantity]',
                                    $position->getQuantity(),
                                    ['id' => 'cart_' . $positionId, 'class' => 'form-control text-center position-count']
                                ); ?>
                                    <div class="frog"></div>
                                </div>
                                <div class="input-group-btn">
                                    <button class="btn btn-default cart-quantity-increase" type="button" data-max-count="<?php echo $position->quantity;?>" data-target="#cart_<?php echo $positionId; ?>">+</button>
                                </div>
                            </div>
                        </td>
                        <td class="col-sm-2 text-center">
                            <strong>
                                <span class="position-price"><?php echo $position->getPrice(); ?></span>
                                <?php echo Yii::t("CartModule.cart", "RUB"); ?>
                            </strong>
                        </td>
                        <td class="col-sm-2 text-center">
                            <strong>
                                <span class="position-sum-price"><?php echo $position->getSumPrice(); ?></span>
                                <?php echo Yii::t("CartModule.cart", "RUB"); ?>
                            </strong>
                        </td>
                        <td class="col-sm-1 text-right">
                            <button type="button" class="btn btn-danger cart-delete-product" data-position-id="<?php echo $positionId; ?>" data-target="#cart_<?php echo $positionId; ?>">
                                <span class="glyphicon glyphicon-remove"></span>
                                <!-- <i class="fa fa-close"></i> -->
                            </button>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="2">
                        <h5><?php echo Yii::t("CartModule.cart", "Subtotal"); ?></h5>
                    </td>
                    <td>  </td>
                    <td colspan="2" style="text-align: right;">
                        <h4>
                            <strong id="cart-full-cost">
                                <?php echo Yii::app()->cart->getCost(); ?>
                            </strong>
                            <?php echo Yii::t("CartModule.cart", "RUB"); ?>
                        </h4>
                    </td>
                </tr>
                <?php //if (Yii::app()->hasModule('coupon')): ?>
                    <!-- <tr>
                        <td colspan="5" class="coupons">
                            <p>
                                <b><?php //echo Yii::t("CartModule.cart", "Coupons"); ?></b>
                            </p>
                            <div class="row">
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <input id="coupon-code" type="text" class="form-control">
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="button" id="add-coupon-code"><?php //echo Yii::t("CartModule.cart", "Add coupon"); ?></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <?php //foreach ($coupons as $coupon): ?>
                                        <span class="label alert alert-info coupon" title="<?php //echo $coupon->name; ?>">
                                            <?php //echo $coupon->code; ?>
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <?php //echo CHtml::hiddenField(
                                                // "Order[couponCodes][{$coupon->code}]",
                                                // $coupon->code,
                                                // [
                                                //     'class' => 'coupon-input',
                                                //     'data-code' => $coupon->code,
                                                //     'data-name' => $coupon->name,
                                                //     'data-value' => $coupon->value,
                                                //     'data-type' => $coupon->type,
                                                //     'data-min-order-price' => $coupon->min_order_price,
                                                //     'data-free-shipping' => $coupon->free_shipping,
                                                // ]
                                            //);?>
                                        </span>
                                    <?php //endforeach; ?>
                                </div>
                            </div>
                        </td>
                    </tr> -->
                <?php //endif; ?>
                <tr>
                    <td colspan="5">
                    <?php if(!empty($deliveryTypes)):?>
                        <fieldset>
                            <div class="form-group">
                                <label class="control-label" for="radios">
                                    <b><?php echo Yii::t("CartModule.cart", "Delivery method"); ?></b>
                                </label>
                                <div class="controls">
                                    <?php foreach ($deliveryTypes as $key => $delivery): ?>
                                        <label class="radio" for="delivery-<?php echo $delivery->id; ?>">
                                            <input type="radio" name="Order[delivery_id]" id="delivery-<?php echo $delivery->id; ?>"
                                                   value="<?php echo $delivery->id; ?>"
                                                   data-price="<?php echo $delivery->price; ?>"
                                                   data-free-from="<?php echo $delivery->free_from; ?>"
                                                   data-available-from="<?php echo $delivery->available_from; ?>"
                                                   data-separate-payment="<?php echo $delivery->separate_payment; ?>">
                                            <?php echo $delivery->name; ?>
                                            <?php if($delivery->price!=0): ?> 
                                                - <?php echo $delivery->price; ?>
                                                <?php echo Yii::t("CartModule.cart", "RUB"); ?>
                                            <?php endif; ?>
                                            (
                                            <?php if($delivery->available_from): ?>
                                                <?php echo Yii::t("CartModule.cart", "available from"); ?>
                                                <?php echo $delivery->available_from; ?>
                                                <?php echo Yii::t("CartModule.cart", "RUB"); ?>;
                                            <?php endif; ?>
                                            <?php if($delivery->free_from): ?>
                                                <?php echo Yii::t("CartModule.cart", "free from"); ?> <?php echo $delivery->free_from; ?>
                                                <?php echo Yii::t("CartModule.cart", "RUB"); ?>; 
                                            <?php endif; ?>
                                            )
                                            <?php echo($delivery->separate_payment ? Yii::t("CartModule.cart", "Pay separately") : ""); ?>
                                        </label>
                                        <div class="text-muted">
                                            <?php echo $delivery->description; ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        </fieldset>
                    <?php else:?>
                        <div class="alert alert-danger">
                            <?php echo Yii::t("CartModule.cart", "Delivery method aren't selected! The ordering is impossible!") ?>
                        </div>
                    <?php endif;?>   
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <h5>
                            <?php echo Yii::t("CartModule.cart", "Delivery price"); ?>
                        </h5>
                    </td>
                    <td>  </td>
                    <td colspan="2" style="text-align: right;">
                        <h4><strong id="cart-shipping-cost">0</strong> <?php echo Yii::t("CartModule.cart", "RUB"); ?></h4>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><h4><?php echo Yii::t("CartModule.cart", "Total"); ?></h4></td>
                    <td>  </td>
                    <td colspan="2" style="text-align: right;">
                        <h4><strong id="cart-full-cost-with-shipping">0</strong> <?php echo Yii::t("CartModule.cart", "RUB"); ?></h4>
                    </td>
                </tr>
                <tr>
                    <td colspan="5">
                        <table class="col-sm-12 order-receiver">
                            <thead>
                                <tr>
                                    <th>
                                        <?php echo Yii::t("CartModule.cart", "Address"); ?>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div>
                                            <?php echo $form->errorSummary($order); ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            <?php echo $form->labelEx($order, 'name'); ?>
                                            <?php echo $form->textField($order, 'name', ['class' => 'form-control']); ?>
                                            <?php echo $form->error($order, 'name'); ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <?php echo $form->labelEx($order, 'phone'); ?>
                                            <?php echo $form->textField($order, 'phone', ['class' => 'form-control']); ?>
                                            <?php echo $form->error($order, 'phone'); ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div>
                                            <?php echo $form->labelEx($order, 'email'); ?>
                                            <?php echo $form->emailField($order, 'email', ['class' => 'form-control']); ?>
                                            <?php echo $form->error($order, 'email'); ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div>
                                            <?php echo $form->labelEx($order, 'address'); ?>
                                            <?php echo $form->textField($order, 'address', ['class' => 'form-control']); ?>
                                            <?php echo $form->error($order, 'address'); ?>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <?php echo $form->labelEx($order, 'comment'); ?>
                                        <?php echo $form->textArea($order, 'comment', ['class' => 'form-control']); ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>

                </tr>
                <tr>
                    <td colspan="5" style="text-align: right;">
                        <a href="<?php echo Yii::app()->createUrl('store/catalog/index'); ?>" class="btn btn-default pull-left">
                            <?php echo Yii::t("CartModule.cart", "Back to catalog"); ?>
                        </a>
                        <button type="submit" class="btn btn-success pull-right">
                            <?php echo Yii::t("CartModule.cart", "Create order and proceed to payment"); ?>
                        </button>
                    </td>
                </tr>
            </tbody>
        </table>
    <?php endif; ?>
    <?php $this->endWidget(); ?>
</div>
