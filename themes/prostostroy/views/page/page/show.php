<?php
/* @var $model Page */
/* @var $this PageController */
?>
<?php if ($model->layout): ?>
    <?php $this->layout = "//layouts/{$model->layout}"; ?>
<?php endif; ?>

<?php
    $mainAssets = $this->mainAssets;
    $this->title = [$model->title, Yii::app()->getModule('yupe')->siteName];
    $this->breadcrumbs = $this->getBreadCrumbs();
    $this->description = $model->description ?: Yii::app()->getModule('yupe')->siteDescription;
    $this->keywords = $model->keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>
<h1 class="page-title">
    <?php echo $model->title; ?>
</h1>
    <?php $this->widget('zii.widgets.CBreadcrumbs',
    [
        'links'=>$this->breadcrumbs,
        'encodeLabel'=>false,
        'tagName'=>'div',
        'separator'=>'//',
        'htmlOptions'=>[
                        'class'=>'breadcrumb'
                        ]
    ])
?>
<!-- </div> -->
<div class="page-content">
<?php echo $model->body; ?>
</div>


