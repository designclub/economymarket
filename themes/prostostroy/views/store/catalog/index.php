<?php

$mainAssets = $this->mainAssets;

if(YII_DEBUG){
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/catalog-main-page.css');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store/store.js');
}else{
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/store-index.min.css');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store/store.min.js');
}
$this->title = ['Каталог продукции',Yii::app()->getModule('yupe')->siteName];
$this->description = '';
$this->keywords = '';
/* @var $category StoreCategory */

$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog")];

?>
<h2><?= Yii::t("StoreModule.store", "Catalog"); ?></h2>
<?php $this->widget('zii.widgets.CBreadcrumbs',
        [
            'links'=>$this->breadcrumbs,
            'encodeLabel'=>false,
            'tagName'=>'div',
            'separator'=>'//',
            'htmlOptions'=>[
                            'class'=>'breadcrumb'
                            ]
        ])
?>
<div class="category">
    <?php $this->widget('yupe\widgets\YFlashMessages'); ?>

    <?php 
    $this->widget(
        'bootstrap.widgets.TbListView',
        [
            'dataProvider' => $categories,
            'itemView' => '_category-item',
            'summaryText' => '',
            'enableHistory' => true,
            'cssFile' => false,
            'pager' => [
                'cssFile' => false,
                'htmlOptions' => ['class' => 'pagination'],
                'header' => '',
                'firstPageLabel' => '',
                'lastPageLabel' => '',
                'nextPageLabel' => '',
                'prevPageLabel' => '',
            ],

        ]
    ); ?>
</div>

