<div class="product__item" style="height: auto; margin-bottom: 15px;">
	<div style="height: 235px;">
		<?php echo CHtml::image($data->image . '/0/140.jpg', '', ['class' => 'product__item_image']); ?>
		<?php echo CHtml::link($data->name, $data->getUrl(), ['class' => 'product__item_link']); ?>
		<p class="product__item_price"><?php echo explode(".", $data->price)[0]; ?> руб.
			<?php if (Yii::app()->hasModule('carta')): ?>
				<a href="#" style="color: #000; font-size: 15px;" class="quick-add-product-to-cart" data-product-id="<?= $data->id; ?>" data-cart-add-url="<?= Yii::app()->createUrl('/cart/cart/add');?>"><i class="glyphicon glyphicon-shopping-cart"></i></a>
			<?php endif; ?>
		</p>
	</div>
	<form action="<?= Yii::app()->createUrl('cart/cart/add'); ?>" method="post">
		<input type="hidden" name="Product[id]" value="<?= $data->id; ?>"/>
		<?= CHtml::hiddenField(
			Yii::app()->getRequest()->csrfTokenName,
			Yii::app()->getRequest()->csrfToken
		); ?>
		<?php if (Yii::app()->hasModule('order')): ?>
			<div class="row">
				<div class="" style="float: left; width: 50%;">
					<div class="input-group">
						<div class="input-group-btn">
							<button class="btn btn-default product-quantity-decrease" type="button">-
							</button>
						</div>
						<input type="text" class="text-center form-control product-quantity" value="1" name="Product[quantity]"/>
						<div class="input-group-btn">
							<button class="btn btn-default product-quantity-increase" type="button">+
							</button>
						</div>
					</div>
				</div>
				<div class="" style="float: left; width: 50%;">
					<button class="fa fa-shopping-cart btn btn-success quick-add-product-to-cart" 
							data-loading-text="<i style='position:absolute;' class='fa fa-upload'></i>" style="height: 34px; background-color: #4d63ff; border-color: #01149D;">
					</button>
				</div>
			</div>
		<?php endif; ?>
	</form>
</div>