<?php

$mainAssets = $this->mainAssets;

if(YII_DEBUG){
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/store-index.css');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store/store.js');
}else{
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/store-index.min.css');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store/store.min.js');
} 

/* @var $category StoreCategory */

$this->title = ['Поиск по каталогу', Yii::app()->getModule('yupe')->siteName];
$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog") => ['/store/catalog/index'], 'Поиск по каталогу'];

?>

<div class="store-category-description">
   
    <div class="lint">
        <h1 class="store-category-title">
            <?php if($category):?>
                <?php echo Yii::t('StoreModule.catalog', 'Search in category "{category}"', ['{category}' => $category->name]); ?>
            <?php else:?>
                <?php echo Yii::t("StoreModule.store", "Search");?>
            <?php endif;?>
        </h1>
         <?php $this->widget('zii.widgets.CBreadcrumbs',
        [
            'links'=>$this->breadcrumbs,
            'encodeLabel'=>false,
            'tagName'=>'div',
            'separator'=>'//',
            'htmlOptions'=>[
                            'class'=>'breadcrumb'
                            ]
                ])
        ?>
    </div>
</div>
    
<?php $this->widget(
    'bootstrap.widgets.TbListView',
    [
        'dataProvider' => $dataProvider,
        'itemView' => '_view1',
        'summaryText' => '',
        'enableHistory' => true,
        'cssFile' => false,
        'pager' => [
            'cssFile' => false,
            'htmlOptions' => ['class' => 'pagination'],
            'header' => '',
            'firstPageLabel' => '&lt;&lt;',
            'lastPageLabel' => '&gt;&gt;',
            'nextPageLabel' => '&gt;',
            'prevPageLabel' => '&lt;',
        ],
        'sortableAttributes' => [
            'sku',
            'name',
            'price'
        ],
    ]
); ?>


