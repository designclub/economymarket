<?php

/* @var $product Product */
$mainAssets = $this->mainAssets;


if(YII_DEBUG){
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/product-card.css');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/components/simpleGal/jquery.simpleGal.js');
    Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getTheme()->getAssetsUrl() . '/js/store/imagezoom.js');
    Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getTheme()->getAssetsUrl() . '/js/store/store.js');
}else{
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/product.min.css');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/components/simpleGal/jquery.simpleGal.min.js');
    Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getTheme()->getAssetsUrl() . '/js/store/imagezoom.js');
    Yii::app()->getClientScript()->registerScriptFile(Yii::app()->getTheme()->getAssetsUrl() . '/js/store/store.min.js');

}

$this->pageTitle = [$product->getMetaTitle(), Yii::app()->getModule('yupe')->siteName];
$this->description = $product->getMetaDescription();
$this->keywords = $product->getMetaKeywords();


$this->breadcrumbs = array_merge(
    [Yii::t("StoreModule.store", 'Catalog') => ['/store/catalog/index']],
    $product->mainCategory ? $product->mainCategory->getBreadcrumbs(true) : [],
    [CHtml::encode($product->name)]
);
?>
<?php 
    $diff=0;
    if(Yii::app()->cart->contains('product_'.$product->id.'_'))
        $diff = Yii::app()->cart->itemAt('product_'.$product->id.'_')->getQuantity();
?>

 <?php //Определяем есть ли товар на складе по параметру in_stock
    if (($product->in_stock>0) && ($product->in_stock!==null))
        $inStock='Достаточно';
    else  $inStock='Нет в наличии';

     $countryFrom='Китай'; //По умолчанию китай, иначе обмночка !
     if(isset($product->country_id)){
        $countryFrom=Country::model()->findByPk($product->country_id)->name;
     }

     //Получение категории
     $series="Различные товары"; 
     if (isset($product->category_id)){
        $series=StoreCategory::model()->findByPk($product->category_id)->name;
     }

     //Вес, всё ясно
    $weight="-"; 
     if (isset($product->weight)){
        $weight=intval($product->weight);
        $weight.=' грамм';
     }

      $price="-"; 
     if (isset($product->price)){
        $price=intval($product->price);
        $price.=' руб.';
     }

     //функция для проверки аттрибута 
     function checkAttr($attribute){
        if(($attribute === null) || (empty($attribute))){
            return '-';
        }else if(is_int($attribute)){
            $attribute.=' шт';
        }
        return $attribute;
     }

      ?>       

 <?php $this->widget('zii.widgets.CBreadcrumbs',
        [
            'links'=>$this->breadcrumbs,
            'encodeLabel'=>false,
            'tagName'=>'div',
            'separator'=>'//',
            'htmlOptions'=>[
                            'class'=>'breadcrumb'
                            ]
        ])
?>
<script>
    
$('[data-imagezoom]').imageZoom({
// image magnifier background color
cursorcolor:'255,255,255',
// opacity
opacity:0.5,
// cursor type
cursor:'crosshair',
// z-index
zindex:2147483647,
// zoom view size
zoomviewsize:[1600,1600],
// zoom view position. left or right
zoomviewposition:'right',
// zoom view margin
zoomviewmargin:10,
// zoom view border
zoomviewborder:'1px solid #000',
// zoom level
magnification: 2
});

</script>

<div class="product-cart">
    <div class="product-left-column col-xs-6">
        <div class="image-preview">
            <img src="https://www.sima-land.ru/api/GetImageSource?id=<?php echo $product->sku;?>&n=0&username=dc@dc56.ru&password=dcadmin" id="main-image" data-imagezoom-zoom="true" style="width: 520px; height: 520px;">
            <!-- <img src="<?php //echo $product->image.'/0/400.jpg'; ?>" alt="" class="" id="main-image"> -->
        </div>
  <!--       <div class="thumbnails">
            <a href="<?php/* echo $product->image.'/0/400.jpg'; ?>" class="thumbnail">
                <img src="<?php echo $product->image.'/0/400.jpg'; */?>"/>
            </a>
            <?php/* foreach ($product->getImages() as $key => $image): { ?>
                <a href="<?php echo $product->image.'/0/400.jpg'; ?>" class="thumbnail">
                    <img src="<?php $product->image.'/0/400.jpg'; ?>"/>
                </a>
            <?php } endforeach; */?>
        </div> -->
        
    </div>
    

    <div class="product-right-column col-xs-6">
        <div class="product-name">
        <h1>
            <?php echo CHtml::encode($product->name); ?>
        </h1>
        </div>
        <table class="rightTable">
            <tr>
            <td>Артикул</td><td><?php echo $product->sku; ?></td>
            </tr>

            <tr>
            <td>Остаток на складе</td><td><?php echo CHtml::encode($inStock); ?></td>
            </tr>

            <tr>
            <td>Страна производитель</td><td><?php echo CHtml::encode($countryFrom); ?></td>
            </tr>

            <tr>
            <td>Серия</td><td><?php echo CHtml::encode($series); ?></td>
            </tr>

            <tr>
            <td>Размер</td><td><?php echo CHtml::encode(checkAttr($product->attribute('size_text'))); ?></td>
            </tr>

            <tr>
            <td>Материал</td><td><?php echo CHtml::encode(checkAttr($product->attribute('material_text'))); ?></td>
            </tr>

            <tr>
            <td>В боксе</td><td><?php echo CHtml::encode(checkAttr($product->attribute('in_set_qty'))); ?></td>
            </tr>

            <tr>
            <td>Упаковано в</td><td><?php echo CHtml::encode(checkAttr($product->attribute('box_type'))); ?></td>
            </tr>

            <tr>
            <td>Фасовка</td><td><?php echo CHtml::encode(checkAttr($product->attribute('prepacking_qty'))); ?></td>
            </tr>

            <tr>
            <td>Вес</td><td><?php echo CHtml::encode($weight); ?></td>
            </tr>
        </table>
        <div class="leftManipul">
        	<div class="price">
        		<p class="thePrice"><?php echo $price; ?></p>
        		<p class="pricePer">Стоимость за штуку</p>
        	</div>
        	<form action="<?= Yii::app()->createUrl('cart/cart/add'); ?>" method="post">
                <input type="hidden" name="Product[id]" value="<?= $product->id; ?>"/>
                <?= CHtml::hiddenField(
                    Yii::app()->getRequest()->csrfTokenName,
                    Yii::app()->getRequest()->csrfToken
                ); ?>
                <table class="table table-condensed">
                    <?php foreach ($product->getVariantsGroup() as $title => $variantsGroup): { ?>
                        <tr>
                            <td style="padding: 0;">
                                <?= CHtml::encode($title); ?>
                            </td>
                            <td>
                                <?=
                                CHtml::dropDownList(
                                    'ProductVariant[]',
                                    null,
                                    CHtml::listData($variantsGroup, 'id', 'optionValue'),
                                    ['empty' => '', 'class' => 'form-control', 'options' => $product->getVariantsOptions()]
                                ); ?>
                            </td>
                        </tr>
                    <?php } endforeach; ?>
                </table>
   

                <?php if (Yii::app()->hasModule('order')): ?>
                    <div class="row">
                        <div class="">
                            <div class="input-group">
                                <div class="input-group-btn">
                                    <button class="btn btn-default product-quantity-decrease" type="button">-
                                    </button>
                                </div>
                                <input type="text" class="text-center form-control" value="1"
                                       name="Product[quantity]" id="product-quantity"/>

                                <div class="input-group-btn">
                                    <button class="btn btn-default product-quantity-increase" type="button">+
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="">
                            <button class="fa fa-shopping-cart btn btn-success pull-left fa-2x" id="add-product-to-cart"
                                    data-loading-text="<i style='position:absolute;' class='fa fa-upload'></i>"> <!-- Lol -->
                                
                            </button>
                        </div>
                    </div>
                <?php endif; ?>
			</form>
		</div>
        <div class="descriptionProduct">
            <p><?php 
	            if ($product->description!=null)
	                echo $product->description; 
	            else echo 'Для данного товара нет описания'; 
	            ?>
            </p>
        </div>
        <?php //print_r($product->category_id) ; ?>

        </div>
  
    </div>

    
<!-- 
                        <div class="row">
        <?php //$this->widget('application.modules.store.widgets.LinkedProductsWidget', ['product' => $product, 'code' => null,]); */?>
    </div> -->

<!-- 
        <div class="product-sku">
            <?php/* echo Yii::t("StoreModule.product", "SKU"); ?>: <?php echo CHtml::encode($product->sku); ?>
        </div>
        <div class="product-brend">
            Бренд: <strong><?php echo CHtml::encode($product->getProducerName()); ?></strong>
        </div>


        <form action="<?php echo Yii::app()->createUrl('cart/cart/add'); ?>" method="post" class="product-form">
            <input type="hidden" name="Product[id]" value="<?php echo $product->id; ?>"/>
            <?php echo CHtml::hiddenField(
                Yii::app()->getRequest()->csrfTokenName,
                Yii::app()->getRequest()->csrfToken
            ); ?>
            <!-- <table class="table table-condensed">
                <?php //foreach ($product->getVariantsGroup() as $title => $variantsGroup): { ?>
                    <tr>
                        <td style="padding: 0;">
                            <?php //echo CHtml::encode($title); ?>
                        </td>
                        <td>
                            <?php
                            // echo
                            // CHtml::dropDownList(
                            //     'ProductVariant[]',
                            //     null,
                            //     CHtml::listData($variantsGroup, 'id', 'optionValue'),
                            //     ['empty' => '', 'class' => 'form-control', 'options' => $product->getVariantsOptions()]
                            // ); ?>
                        </td>
                    </tr>
                <?php //} endforeach; ?>
            </table> -->
            <div>
                <?php if(round($product->getBasePrice(), 2) !== round($product->getResultPrice(), 2)): ?>
                    <div class="product-discount">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             viewBox="0 0 130 26" enable-background="new 0 0 130 26" xml:space="preserve" width="130">
                            <path d="M117,0H0v26h117l13-13L117,0z M111.4,16.5c-1.9,0-3.4-1.6-3.4-3.5s1.5-3.5,3.4-3.5c1.9,0,3.4,1.6,3.4,3.5
    S113.3,16.5,111.4,16.5z"/>
                            <text transform="matrix(1 0 0 1 16 17)">скидка <?php echo $product->discount ? number_format($product->discount,0).'%':''; ?></text>
                        </svg>
                        <!-- <span id="result-price">cкидка <?php echo $product->discount; ?></span> -->
                    </div>
                <?php endif; ?>
                <div class="product-result-price"><?php echo number_format($product->getResultPrice(),0,'.',''); ?><span class="currency">рублей</span></div>
                <input type="hidden" id="base-price" value="<?php echo round($product->getResultPrice(), 2); ?>"/>
            </div>

            <?php $quantityForUser = $product->quantity - $diff; ?>
            <div class="product-quantity">
                Осталось: <strong><span id="quantity"><?php echo $quantityForUser == 0 ? 'На складе товар отсутствует': $quantityForUser.' штук' ; ?><span></strong>
            </div>
            <?php if (Yii::app()->hasModule('order') && $product->quantity - $diff  > 0 ): ?>
                <div class="input-group col-xs-11" id="order-inputs">
                    <div class="input-group-btn">
                        <button class="btn btn-default product-quantity-decrease" type="button">-
                        </button>
                    </div>
                    <input type="text" class="text-center form-control" value="1"
                           name="Product[quantity]" id="product-quantity"/>

                    <div class="input-group-btn">
                        <button class="btn btn-default product-quantity-increase" type="button" data-max-count='<?php echo $product->quantity -  $diff;?>'>+
                        </button>
                    </div>
                    <button class="btn btn-success pull-left btn-buy" id="add-product-to-cart" data-loading-text="В корзину">
                        В корзину
                    </button>
                </div>
                
            <?php endif; ?>
        </form>
        <div class="product-description">
            <h4><?php echo Yii::t("StoreModule.store", "Description"); ?></h4>
                <?php echo $product->description; ?>
        </div>
    </div>
    <div class="row">
        <?php //$this->widget('application.modules.store.widgets.LinkedProductsWidget', ['product' => $product, 'code' => null,]); */?>
    </div>
</div> -->



<?php Yii::app()->getClientScript()->registerScript(
    "product-images",
    <<<JS
        $(".thumbnails").simpleGal({
    mainImage: "#main-image"
});
JS
); ?>
