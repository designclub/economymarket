<?php
$mainAssets = $this->mainAssets;

if(YII_DEBUG){
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/catalog-category-page.css');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store/store.js');
}else{
    Yii::app()->getClientScript()->registerCssFile($mainAssets . '/css/store-index.min.css');
    Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store/store.min.js');
}



/* @var $category StoreCategory */

$this->title =  [$category->getMetaTile()];
$this->metaDescription = $category->getMetaDescription();
$this->metaKeywords =  $category->getMetaKeywords();

$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog") => ['/store/catalog/index']];

$this->breadcrumbs = array_merge(
    $this->breadcrumbs,
    $category->getBreadcrumbs(false)
);

?>


<!-- <div class="store-category-description" style="margin-top: 30px;"> -->
    <?php //echo file_get_contents('.'.$mainAssets.'/svg/store-category-description-mask.svg') ?>
    <!-- <div class="cover-image"> -->
       <!--  <svg id="coverImage" viewBox="0 0 800 140">
             <image xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="<?php //echo strip_tags($category->short_description);?>"  width="800" height="140" clip-path="url(#clipping)"></image>
        </svg> -->
   <!--  </div>
    <div class="lint">

        <div class="give-me-more"> -->
            <?php /*$this->widget('zii.widgets.CBreadcrumbs',
                [
                    'links'=>$this->breadcrumbs,
                    'encodeLabel'=>false,
                    'tagName'=>'ul',
                    'separator'=>'',
                    // 'activeLinkTemplate'=>'<li><a href="{url}"><div class="separator">'.file_get_contents('.'.$mainAssets.'/svg/icon-arrow.svg').'</div>{label}</a></li>',
                    // 'inactiveLinkTemplate'=>'<li class="active"><div class="separator">'.file_get_contents('.'.$mainAssets.'/svg/icon-arrow.svg').'</div>{label}</li>',
                    'htmlOptions'=>[
                                    'class'=>'breadcrumb'
                                    ]
                ]) */
            ?>
        <!-- </div>
    </div>  -->
<!--     <div class="store-category-description-text">
    <?php //echo $category->description; ?>
</div> -->
<!-- </div> -->


<?php if($childrens = $category->children):?>
    <div class="category__item" style="margin-bottom: 30px;">
        <h2 class="category__item_title">
            <?php echo $category->name; ?>
        </h2>
         <?php $this->widget('zii.widgets.CBreadcrumbs',
        [
            'links'=>$this->breadcrumbs,
            'encodeLabel'=>false,
            'tagName'=>'div',
            'separator'=>'//',
            'htmlOptions'=>[
                            'class'=>'breadcrumb'
                            ]
                ])
        ?>
        <?php foreach ($childrens as $key => $child): ?>
            <?php if (true) : ?>
                <div class="subcat__item">
                    <a href="<?php echo $child->getUrl(); ?>" class="subcat__item_link with-image">
                        <img src="<?php echo $child->image; ?>" class="subcat__item_image"/>
                    </a>
                    <p class="subcat__item_descr">
                        <a href="<?php echo $child->getUrl(); ?>" class="subcat__item_link">
                            <?php echo trim($child->name); ?>
                        </a>
                        <!-- <span class="subcat__item_count">
                             <?php //echo mt_rand(1, 1500) ?>
                        </span> -->
                    </p>
                </div>
            <?php else : ?>
                <span class="subcat__item_without-image">
                    <a href="<?php echo $child->getUrl(); ?>" class="subcat__item_link">
                         <?php echo trim($child->name); ?>
                    </a>
                    <!-- <span class="subcat__item_count">
                        <?php //echo mt_rand(1, 1500) ?>
                    </span> -->
                </span>
            <?php endif; ?>
        <?php endforeach ?>

    </div>
<?php else: ?>
    <h1 class="products__category_title"> <?php echo $category->name ?> </h1>
    <?php $this->widget('zii.widgets.CBreadcrumbs',
        [
            'links'=>$this->breadcrumbs,
            'encodeLabel'=>false,
            'tagName'=>'div',
            'separator'=>'//',
            'htmlOptions'=>[
                            'class'=>'breadcrumb'
                            ]
                ])
        ?>
    <div class="products__list">

    <?php $this->widget(
        'bootstrap.widgets.TbListView',
        [
            'dataProvider' => $dataProvider,
            'itemView' => '_product-item',
            'summaryText' => '',
            'enableHistory' => true,
            'cssFile' => false,
            'pager' => [
                'cssFile' => false,
                'htmlOptions' => ['class' => 'pagination'],
                'header' => '',
                'firstPageLabel' => '&lt;&lt;',
                'lastPageLabel' => '&gt;&gt;',
                'nextPageLabel' => '&gt;',
                'prevPageLabel' => '&lt;',
            ],
            'sortableAttributes' => [
                // 'sku',
                'name',
                'price'
            ],
        ]
    ); ?>
</div>
<?php endif; ?>
