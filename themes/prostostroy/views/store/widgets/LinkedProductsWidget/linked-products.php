<?php /* @var $dataProvider CActiveDataProvider */ ?>
<h3>
    <?= Yii::t('StoreModule.product', 'Linked products') ?>
</h3>

<?php $this->widget(
    'application.widgets.YListView',
    [
        'dataProvider' => $dataProvider,
        'template' => '{items}',
        'itemView' => '_view',
        'cssFile' => false,
        'pager' => false,
    ]
); ?>

