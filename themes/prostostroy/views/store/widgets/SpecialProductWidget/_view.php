<?php $productUrl = Yii::app()->createUrl('/store/catalog/show', ['name' => CHtml::encode($data->slug)]); ?>
<div class="item-block">
    <div class="item">
        <div class="photo">
            <a href="<?php echo $productUrl; ?>">
                <img src="<?php echo $data->getImageUrl(170, 170); ?>"/>
            </a>
        </div>
        <div class="info">
            <div class="price">
                <?php echo $data->getResultPrice(); ?> Р.
            </div>
            <div class="name">
                <a href="<?php echo $productUrl; ?>"><?php echo CHtml::encode($data->getName()); ?></a>
            </div>
            <?php if (Yii::app()->hasModule('cart')): ?>
                <a href="<?php echo $productUrl; ?>" class="btn btn-buy">купить</a>
            <?php endif; ?>
        </div>
    </div>
</div>
