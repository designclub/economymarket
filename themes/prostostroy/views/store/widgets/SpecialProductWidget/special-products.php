<?php $this->widget(
        'application.widgets.YListView',
        [
            'dataProvider' => $dataProvider,
            'itemView' => '_view',
            'summaryText' => '',
            'template'=>'{items}',
            'cssFile' => false,
            'emptyText'=>'Спец.предложений на данный момент нет.',
        ]
    ); ?>