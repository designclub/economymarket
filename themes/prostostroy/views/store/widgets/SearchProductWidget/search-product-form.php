<?php $mainAssets = $this->controller->mainAssets; ?>
<section class="store-filter">
    <?php $form = $this->beginWidget(
        'bootstrap.widgets.TbActiveForm',
        [
            'action' => ['/store/catalog/search'],
            'method' => 'get',
            'id'=>'search-form',
        ]
    )?>
    <div class="input-group">
        <?php echo $form->textField($searchForm, 'q', ['class' => 'form-control', 'placeholder' => 'введите название товара', 'autocomplete'=>'off' ]); ?>
        <?php echo $form->hiddenField($searchForm, 'category')?>
        <button type="submit" class="btn btn-default"><?php echo file_get_contents('.'.$mainAssets.'/svg/icon-loupe.svg'); ?></button>
    </div>
    <?php 
        if(YII_DEBUG){
            Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store/search.js'); 
        }else{
            Yii::app()->getClientScript()->registerScriptFile($mainAssets . '/js/store/search.min.js'); 
        }
    ?>
    <?php $this->endWidget(); ?>
</section>
