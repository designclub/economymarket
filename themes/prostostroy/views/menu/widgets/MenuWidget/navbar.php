<?php
    $this->widget('zii.widgets.CMenu', [
        'items' => $this->params['items'],
        'encodeLabel' => false,
        'itemCssClass'	=> 'item',
        'htmlOptions'=>[ 
            'class'=>'menu'
        ]
    ]);
?>