module.exports = 
{
	minified:{
	    css: ["web/css/*.min.css"],
	    js: ["web/js/**/*.min.js"]
	}
}