module.exports =
{
    raster_icons_mixins: {
        src: 'source/sprites/raster_icons/*.png',
        dest: 'web/sprites/raster_icons-sprite.png',
        imgPath: '../sprites/raster_icons-sprite.png',
        destCss: 'source/sass/modules/Sprites/_raster_icons-sprite.scss',
        cssTemplate: function (params) {
            var spriteClassName = 'ri';
            var result = '@mixin '+spriteClassName+' {\n\tdisplay: inline-block;\n\tbackground-image: url('+params.items[0].image+');\n\tbackground-repeat: no-repeat;\n\twidth: ' + params.items[0].px.width + ';\n\t' +
                    'height: ' + params.items[0].px.height + ';' +'\n}\n';
            for (var i = 0, ii = params.items.length; i < ii; i += 1) {
                result += '@mixin '+spriteClassName+'-' + params.items[i].name + '{' +
                    'background-position: ' + params.items[i].px.offset_x + ' ' + params.items[i].px.offset_y + ';' +'}\n';
            }
            return result;
        }
    },
    raster_icons_styles: {
        src: 'source/sprites/raster_icons/*.png',
        dest: 'web/sprites/raster_icons-sprite.png',
        imgPath: '../sprites/raster_icons-sprite.png',
        destCss: 'web/sprites/raster_icons-sprite.css',
        cssTemplate: function (params) {
            var spriteClassName = 'ri';
            var result = '.'+spriteClassName+' {\n\tdisplay: inline-block;\n\tbackground-image: url('+params.items[0].image+');\n\tbackground-repeat: no-repeat;\n\twidth: ' + params.items[0].px.width + ';\n\t' +
                    'height: ' + params.items[0].px.height + ';' +'\n}\n';
            for (var i = 0, ii = params.items.length; i < ii; i += 1) {
                result += '.'+spriteClassName+'-' + params.items[i].name + '{' +
                    'background-position: ' + params.items[i].px.offset_x + ' ' + params.items[i].px.offset_y + ';' +'}\n'
            }
            return result;
        }
    },
    reason_styles: {
        algorithmOpts: {sort: true},
        src: 'source/sprites/reason/*.png',
        dest: 'web/sprites/reason-sprite.png',
        imgPath: '../sprites/reason-sprite.png',
        retinaSrcFilter: 'source/sprites/reason/*2x.png',
        retinaDest: 'web/sprites/reason-sprite@2x.png',
        retinaImgPath: '../sprites/reason-sprite@2x.png',
        destCss: 'source/sass/modules/Sprites/_reason-sprite.scss',
        cssTemplate: 
        function (params) {
            var spriteClassName = 'reason-icon';
            var result = '.'+spriteClassName+' {\n\tdisplay: inline-block;\n\tbackground-image: url('+params.items[0].image+');\n\tbackground-repeat: no-repeat;\n\twidth: ' + params.items[0].px.width + ';\n\t' +
                    'height: ' + params.items[0].px.height + ';' + '\n\tbackground-size:'+params.items[0].px.total_width+' '+params.items[0].px.total_height+';}\n';
            for (var i = 0, ii = params.items.length; i < ii; i += 1) {
                result += '.'+spriteClassName+'-' + params.items[i].name + '{' +
                    'background-position: ' + params.items[i].px.offset_x + ' ' + params.items[i].px.offset_y + ';' +'}\n'
            }
            result += '.'+spriteClassName+' {\n ' + '\t@include retina{\n'+
                '\t\tbackground-image: url('+params.items[0].image.replace(/.png/g,"@2x.png")+');\n\t}\n}';
            return result;
        }
    },
    // reason_styles_retina: {
    //     src: 'source/sprites/reason/@2x/*.png',
    //     dest: 'web/sprites/reason-sprite@2x.png',
    //     destCss: 'source/sass/modules/Sprites/_reason-sprite@2x.scss',
    // },
}