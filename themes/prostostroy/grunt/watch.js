module.exports = 
{
    options: {
        livereload: true,
    },
    sass: {
        files: ['sources/sass/**/*.scss'],
        tasks: ['sass'],
    },
    less: {
        files: ['sources/less/**/*.less'],
        tasks: ['less'],
    },
    configFiles: {
        files: ['Gruntfile.js', 'views/**/*.php', 'web/**/*.js', 'web/**/*.svg'],
        options: {
            reload: true
        }
    }
}