module.exports = 
{
    js: {
        files: [{
            expand: true,
            cwd: 'web/js',
            src: ['**/*.js', '!**/*.min.js'],
            dest: 'web/js',
            ext: '.min.js'
        }]
    },
    components: {
        files: [{
            expand: true,
            cwd: 'web/components',
            src: ['**/*.js', '!**/*.min.js'],
            dest: 'web/components/',
            ext: '.min.js',
            extDot: 'last',
        }]
    }
}