<?php
return [
    'module'    => [
        'class' => 'application.modules.fileswap.FileswapModule',
    ],
    'import'    => [
        'application.modules.fileswap.extensions.phpexcel.XPHPExcel'
    ],
    'component' => [
        'log'   =>  [
            'class'=>'CLogRouter',
            'routes'=>[
                [
                    'class'=>'system.logging.CFileLogRoute',
                    'categories'=>'sse.*',
                    'logFile'=>'fileswap.log',
                    'maxLogFiles'=>100000,
                ],
            ],
        ],
    ],
    'rules'     => [
        '/exchange/index/<key>'=>'exchange1c/default/index',
        '/exchange/test-read/<size:\d+>'=>'exchange1c/default/testRead',
        '/exchange/<action:\w+>'=>'exchange1c/default/<action>',
    ],

];

?>