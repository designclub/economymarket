<?php
Yii::import('application.modules.gallery.models.*');

class CarouselWidget extends yupe\widgets\YWidget
{
    public $idGallery = 1;
    public $view = 'carousel';

    public function run()
    {
        $images = $this->getArrayImages();
        if (!empty($images)) {
            $this->render(
                $this->view,
                [
                    'images' => $images,
                ]
            );
        }
    }

    public function getArrayImages()
    {
        $array = [];
        $model = Gallery::model()->findByPk($this->idGallery);
        if ($model!==null) {
             foreach ($model->images as $key => $value) {
                $array[] = [
                    'image'=>$value->getImageUrl(), //'/uploads/image/'.$value->file,
                    'label'=>$value->alt
                ];
            }
        }
        return $array;
    }
}