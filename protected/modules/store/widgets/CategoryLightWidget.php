<?php

Yii::import('application.modules.store.models.StoreCategory');

class CategoryLightWidget extends yupe\widgets\YWidget
{
    public $parent = 0;

    public $depth = 1;

    public $view = 'category-light-widget';

    public $htmlOptions = [];

    public function run()
    {
        $this->render($this->view,  [
            'tree' => (new StoreCategory())->getCategoriesLight(),
            'htmlOptions' => $this->htmlOptions
        ]);
    }
}
