<?php
/**
 * Отображение для view:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('store')->getCategory() => array(),
        Yii::t('store', 'Страны') => array('/store/countryBackendController/index'),
        $model->name,
    );

    $this->pageTitle = Yii::t('store', 'Страны - просмотр');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('store', 'Управление Странами'), 'url' => array('/store/countryBackendController/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('store', 'Добавить Страну'), 'url' => array('/store/countryBackendController/create')),
        array('label' => Yii::t('store', 'Страну') . ' «' . mb_substr($model->id, 0, 32) . '»'),
        array('icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('store', 'Редактирование Страны'), 'url' => array(
            '/store/countryBackendController/update',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('store', 'Просмотреть Страну'), 'url' => array(
            '/store/countryBackendController/view',
            'id' => $model->id
        )),
        array('icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('store', 'Удалить Страну'), 'url' => '#', 'linkOptions' => array(
            'submit' => array('/store/countryBackendController/delete', 'id' => $model->id),
            'confirm' => Yii::t('store', 'Вы уверены, что хотите удалить Страну?'),
            'csrf' => true,
        )),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('store', 'Просмотр') . ' ' . Yii::t('store', 'Страны'); ?>        <br/>
        <small>&laquo;<?php echo $model->name; ?>&raquo;</small>
    </h1>
</div>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
'data'       => $model,
'attributes' => array(
        'id',
        'name',
),
)); ?>
