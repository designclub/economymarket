<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('store')->getCategory() => array(),
        Yii::t('store', 'Страны') => array('/store/countryBackendController/index'),
        Yii::t('store', 'Управление'),
    );

    $this->pageTitle = Yii::t('store', 'Страны - управление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('store', 'Управление Странами'), 'url' => array('/store/countryBackendController/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('store', 'Добавить Страну'), 'url' => array('/store/countryBackendController/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('store', 'Страны'); ?>
        <small><?php echo Yii::t('store', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?php echo Yii::t('store', 'Поиск Стран');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
    <?php
Yii::app()->clientScript->registerScript('search', "
    $('.search-form form').submit(function () {
        $.fn.yiiGridView.update('country-grid', {
            data: $(this).serialize()
        });

        return false;
    });
");
$this->renderPartial('_search', array('model' => $model));
?>
</div>

<br/>

<p> <?php echo Yii::t('store', 'В данном разделе представлены средства управления Странами'); ?>
</p>

<?php
 $this->widget('yupe\widgets\CustomGridView', array(
'id'           => 'country-grid',
'type'         => 'striped condensed',
'dataProvider' => $model->search(),
'filter'       => $model,
'columns'      => array(
        'id',
        'name',
array(
'class' => 'yupe\widgets\CustomButtonColumn',
),
),
)); ?>
