<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
    $this->breadcrumbs = array(
        Yii::app()->getModule('store')->getCategory() => array(),
        Yii::t('store', 'Страны') => array('/store/countryBackendController/index'),
        Yii::t('store', 'Добавление'),
    );

    $this->pageTitle = Yii::t('store', 'Страны - добавление');

    $this->menu = array(
        array('icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('store', 'Управление Странами'), 'url' => array('/store/countryBackendController/index')),
        array('icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('store', 'Добавить Страну'), 'url' => array('/store/countryBackendController/create')),
    );
?>
<div class="page-header">
    <h1>
        <?php echo Yii::t('store', 'Страны'); ?>
        <small><?php echo Yii::t('store', 'добавление'); ?></small>
    </h1>
</div>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>